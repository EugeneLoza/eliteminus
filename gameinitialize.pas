{ MIT-License

  Copyright (c) 2019-2020 Yevhen Loza

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
}
unit GameInitialize;

interface

implementation

uses SysUtils, Generics.Collections,
  CastleWindow, CastleScene, CastleControls, CastleLog,
  CastleFilesUtils, CastleSceneCore, CastleKeysMouse,
  CastleUIControls, CastleApplicationProperties, CastleComponentSerialize,
  CastleViewport, CastleTransform, CastleVectors, CastleQuaternions,
  CastleCameras, X3DNodes;

type
  TMissileList = specialize TObjectList<TCastleTransform>;

type
  { A camera that stores Position, Direction and Up vectors
    at the moment only TWalkCamera exposes those,
    but we don't want it in space for obvious reasons :) }
  TSpaceNavigation = class(TCastleExamineNavigation)
  public
    { Previous values of Position, Direction and Up vectors }
    SmoothPosition, SmoothDirection, SmoothUp: TVector3;
  end;

var
  { A game window, where the game takes place }
  Window: TCastleWindowBase;
  { SceneManage4 that holds all te objects together }
  Viewport: TCastleViewport;
  { Planet Earth model }
  Earth: TCastleTransform;
  { Ship model }
  Ship: TCastleTransform;
  { Camera following the ship }
  Navigation: TSpaceNavigation;
  { Missiles launched by the palyer }
  Missiles: TMissileList;

procedure WindowUpdate(Container: TUIContainer);
const
  { Ship velocity in space units per second }
  Velocity = 50;
  { How smooth the contols are
    in other words how far the camera lags behind the ship model }
  ControlsSmoothness = 9;
var
  CameraFixedPosition: TVector3;
  M: TCastleTransform;
begin
  for M in Missiles do
  begin
    M.Translation := M.Translation + 10 * Velocity * M.Direction * Container.Fps.SecondsPassed;
    if (M.Translation - Ship.Translation).Length > 1000 then
    begin
      Viewport.Items.Remove(M);
      Missiles.Remove(M);
    end;
  end;

  { Move the ship }
  Ship.Translation := Ship.Translation + Velocity * Ship.Direction * Container.Fps.SecondsPassed;

  { Lag the camera behind the ship }
  { We process the "position" similarly to ship's model
    by adding ship's velocity to it - so that the camera
    lays due to player controls, not due to the ship velocity }
  CameraFixedPosition := Ship.Translation - 12 * Ship.Direction + 4 * Ship.Up;
  Navigation.SmoothPosition := (ControlsSmoothness * Navigation.SmoothPosition + CameraFixedPosition) / (ControlsSmoothness + 1);
  Navigation.SmoothDirection := (ControlsSmoothness * Navigation.SmoothDirection + Ship.Direction) / (ControlsSmoothness + 1);
  Navigation.SmoothUp := (ControlsSmoothness * Navigation.SmoothUp + Ship.Up) / (ControlsSmoothness + 1);
  { Finally, set current camera view to the smoothed values
    I.e. if we don't need smoothing, we can just use
    TExamineCamera and assign exact Position/Diection/Up
    values here, relative to the ship model. }
  Navigation.SetView(Navigation.SmoothPosition, Navigation.SmoothDirection, Navigation.SmoothUp, false);
end;

procedure WindowMotion(Container: TUIContainer; const Event: TInputMotion);
const
  { Touch sensitivity of the touch/mouse motion
    Caution current implementation works only with mouse control }
  TouchSensitivity = 10000;
var
  { We use quaternions to simply "add" ship's current rotation
    relatively to global coordinates system,
    with rotation of control (player input) relative to the screen
    i.e. the camera orientation }
  QStart, QScreenX, QScreenY: TQuaternion;
  { Parameters of mouse motion shift from center of the Window }
  MouseShift, Center: TVector2;
begin
  { Hide mouse cursor every frame }
  Viewport.Cursor := mcNone;
  { Get the center of the window
    we could do it only once after it is open }
  Center := Vector2(Window.Width / 2, Window.Height / 2);
  { Omit any futher actions if cursor is perfectly in the center of the screen }
  if not TVector2.PerfectlyEquals(Event.Position, Center) then
  begin
    { If mouse has been moved, get the mouse shift relative to the screen center }
    MouseShift := Event.Position - Center;
    Window.MousePosition := Center;
  end else
    Exit;

  //quatermion for the ship initial rotation
  QStart := QuatFromAxisAngle(Ship.Rotation);
  //quaternions for the player controls in X and Y axes
  QScreenX := QuatFromAxisAngle(Vector4(Ship.Up, -MouseShift[0] / TouchSensitivity));
  QScreenY := QuatFromAxisAngle(Vector4(TVector3.CrossProduct(Ship.Up, Ship.Direction), -MouseShift[1] / TouchSensitivity));
  //and finally collect those in ship rotation
  Ship.Rotation := (QScreenX * QScreenY * QStart).ToAxisAngle;
  Ship.Rotation := Vector4(Vector3(Ship.Rotation[0], Ship.Rotation[1], Ship.Rotation[2]).Normalize, Ship.Rotation[3]);
end;

procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
var
  MissileNode: TSphereNode;
  MissileRoot: TX3DRootNode;
  MissileScene: TCastleScene;
  MissileTransform: TCastleTransform;
begin
  if Event.EventType = itMouseButton then
  begin
    MissileTransform := TCastleTransform.Create(Viewport);
    MissileNode := TSphereNode.Create;
    MissileNode.FdRadius.Value := 1;
    MissileNode.FdSlices.Value := 1;
    MissileRoot := TX3DRootNode.Create;
    MissileRoot.FdChildren.Add(MissileNode);
    MissileScene := TCastleScene.Create(MissileTransform);
    MissileScene.Load(MissileRoot, true);
    MissileTransform.Add(MissileScene);
    MissileTransform.Translation := Ship.Translation + Ship.Direction * MissileNode.FdRadius.Value;
    MissileTransform.Direction := Ship.Direction;
    Missiles.Add(MissileTransform);
    Viewport.Items.Add(MissileTransform);
  end;
end;

{ One-time initialization of resources. }
procedure ApplicationInitialize;
var
  Ui: TCastleUserInterface;
  { Variables to load scene elements }
  EarthScene, ShipScene, SkyBox: TCastleScene;
begin
  { Assign event that fires every frame }
  Window.OnUpdate := @WindowUpdate;
  { Assign event that fires when the player moves mouse }
  Window.OnMotion := @WindowMotion;
  Window.OnPress := @WindowPress;

  { Adjust container settings,
    e.g. for a scalable UI (adjusts to any window size in a smart way). }
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');

  { Load designed user interface }
  Ui := UserInterfaceLoad('castle-data:/main.castle-user-interface', Window);
  Window.Controls.InsertFront(Ui);

  //find Viewport in the UI design
  Viewport := Window.FindRequiredComponent('Viewport1') as TCastleViewport;

  { Load Earth model and add its parameters }
  Earth := TCastleTransform.Create(Viewport);
  EarthScene := TCastleScene.Create(Earth);
  EarthScene.Load('castle-data:/models/earth.x3d');
  //Add Earth model to Earth transform
  Earth.Add(EarthScene);
  { Set Translation (location) and Scale (size) of the Earth
    Pay attention, that large values result in low accuracy
    i.e. shaky movements of the ship. In order to work with
    larger scenes, like starsystems and glalaxies
    some additional tricks and optimizations should be invented. }
  Earth.Scale := Vector3(1000, 1000, 1000);
  Earth.Translation := Vector3(0, 0, 5000);
  //Finally, add Earth to Viewport
  Viewport.Items.Add(Earth);

  { Load ship model }
  Ship := TCastleTransform.Create(Viewport);
  ShipScene := TCastleScene.Create(Ship);
  ShipScene.Load('castle-data:/models/siar1x.x3d');
  //add ship model to ship translation
  Ship.Add(ShipScene);
  { Set initial direction of the ship facing the Earth
    note, that I don't solve a minor bug here -
    when the game starts, the mouse pointer is not located
    in the middle of the screen, so first OnMotion event
    will cause a "jump" in controls and the ship will be
    oriented semi-randomly. }
  Ship.Direction := Earth.Translation - Ship.Translation;
  //Add the ship to Viewport
  Viewport.Items.Add(Ship);

  { Load Skybox (starry background) }
  SkyBox := TCastleScene.Create(Viewport);
  SkyBox.Load('castle-data:/models/Background.x3d');
  //add skybox to Viewport
  Viewport.Items.Add(SkyBox);
  //pay attention that in order to be visible main light and skybox must be added to the "MainScene" of the Viewport
  Viewport.Items.MainScene := SkyBox;
  //disable cursor over Viewport
  Viewport.Cursor := mcForceNone;
  Missiles := TMissileList.Create(false);

  { Create a camera for the game
    Normally Viewport will automatically create a camera,
    but we need a specific TSpaceCamera with specific parameters,
    so, we create it manually. }
  Navigation := TSpaceNavigation.Create(Application);
  //disable automatic handling of input, we do that manually in OnUpdate and OnMotion
  Navigation.Input := [];
  //and set our camera as the main camera for Viewport
  Viewport.AutoCamera := true;
  Viewport.AutoNavigation := false;
  Viewport.Navigation := Navigation;
end;

initialization
  { Set ApplicationName early, as our log uses it.
    Optionally you could also set ApplicationProperties.Version here. }
  ApplicationProperties.ApplicationName := 'ElieteMinus';

  { Start logging. Do this as early as possible,
    to log information and eventual warnings during initialization. }
  InitializeLog;

  { Initialize Application.OnInitialize. }
  Application.OnInitialize := @ApplicationInitialize;

  { Create and assign Application.MainWindow. }
  Window := TCastleWindowBase.Create(Application);
  Application.MainWindow := Window;

finalization
  Missiles.Free;

end.
